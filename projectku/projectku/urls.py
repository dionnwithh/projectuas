
from django.contrib import admin
from django.urls import path, include

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings

from . views import home, about,contact, detail_artikel
from . autentikasi import akun_login,akun_registrasi,akun_logout

from berita.api import *

urlpatterns = [
    path('admin/', admin.site.urls), 

    path('', home, name='home'),

    path('about', about, name='about'),
    path('contact', contact, name='contact'),
    path('artikel/<slug:slug>', detail_artikel, name='detail_artikel'),

    path('dashboard/', include("berita.urls")),
    
    path('api/kategori/list', api_kategori_list),
    path('api/kategori/detail/<int:id_kategori>', api_kategori_detail),

    path('api/artikel/detail/<int:id_artikel>', api_artikel_detail),

    path('api/artikel/list', api_artikel_list),
    path('api/author/list', api_author_list),


    path('autentikasi/login', akun_login, name="akun_login"),
    path('autentikasi/registrasi', akun_registrasi, name="akun_registrasi"),
    path('autentikasi/logout', akun_logout, name="akun_logout"),

    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('api-auth/', include('rest_framework.urls')),

]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)