from rest_framework import serializers
from django.contrib.auth.models import User
from berita.models import Kategori, Artikel
from pengguna.models import Biodata

class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username','first_name', 'last_name', 'email',]

class BiodataSerializers(serializers.ModelSerializer):
    user = UserSerializers(many=False)
    class Meta:
        model = Biodata
        fields = ['user', 'alamat', 'telpon', 'foto']

class KategoriSerializers(serializers.ModelSerializer):
    class Meta:
        model = Kategori
        fields = ['id', 'nama',]

class ArtikelSerializers(serializers.ModelSerializer):
    author = UserSerializers()
    kategori = KategoriSerializers()
    class Meta:
        model = Artikel
        fields = ['id', 'judul','isi','kategori','author','thumbnail','created_at','slug']